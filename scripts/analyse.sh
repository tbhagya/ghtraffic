#!/bin/bash 
export _JAVA_OPTIONS="-Xmx10g"
mvn -q clean compile exec:java -Dexec.mainClass="nz.ac.massey.ghtraffic.scripts.analyser.AnalyseHttpTransactions"
exit 0