package nz.ac.massey.dataset.exporter;

import com.mongodb.DB;
import com.mongodb.Mongo;
import org.junit.*;
import java.io.FileInputStream;
import java.util.Properties;

public class JsonExporterTest{

    @BeforeClass
    public static void beforeAll() throws Exception
    {
        DB mongoDb;

        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        Mongo mongo = new Mongo(prop.getProperty("host"), Integer.parseInt(prop.getProperty("port")));
        mongoDb = mongo.getDB(prop.getProperty("database"));

        //check whether the collection exists in the db
        boolean collectionExists = mongoDb.collectionExists("issues");

        if(!collectionExists) System.out.print("collection not exist");
        Assume.assumeTrue (!collectionExists);
    }

    @Test
    public void testPOSTTransactionRecord() throws Exception {

    }

}