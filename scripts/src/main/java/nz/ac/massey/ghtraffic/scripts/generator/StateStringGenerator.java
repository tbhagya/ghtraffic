package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates string for state GitHub parameter
 * @author thilini bhagya
 */
public class StateStringGenerator {
    public static final String OPEN = "open";
    public static final String CLOSED = "closed";

}
