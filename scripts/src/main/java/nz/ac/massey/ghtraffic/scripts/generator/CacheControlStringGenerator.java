package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates strings for Cache-Control Http header
 * @author thilini bhagya
 */
public class CacheControlStringGenerator {
    public static final String PRIVATE = "private, max-age=60";
}
