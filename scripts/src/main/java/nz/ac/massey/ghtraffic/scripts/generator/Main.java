package nz.ac.massey.ghtraffic.scripts.generator;

import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.exporter.JSONWriter;
import nz.ac.massey.ghtraffic.scripts.extractor.Filters;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
/**
 * This is the main class that invokes experiments based on user provided argument
 * @author thilini bhagya
 */
public class Main {

    static org.apache.log4j.Logger LOGGER = Logging.getLogger(Main.class);

    public static void main(String args[]) {

        try {

            File directory = new File("DataSet");
            directory.mkdir();

            Options opt = new Options();

            opt.addOption("S", false, "Generate small dataset");
            opt.addOption("M", false, "Generate medium dataset");
            opt.addOption("L", false, "Generate large dataset");

            CommandLineParser parser = new BasicParser();
            CommandLine cmd = parser.parse(opt, args);

            if (cmd.hasOption("S")) {
                File subdirectory = new File("DataSet/Small");
                subdirectory.mkdir();
                Writer sout = new FileWriter(new File(subdirectory, "sdata.json"));
                JSONWriter sj = new JSONWriter();
                sj.createDatafiles(sout, Filters.sFilter());
            }

            if (cmd.hasOption("M")) {
                File subdirectory = new File("DataSet/Medium");
                subdirectory.mkdir();
                Writer mout = new FileWriter(new File(subdirectory,"mdata.json"));
                JSONWriter mj = new JSONWriter();
                mj.createDatafiles(mout, Filters.mFilter());

            }

            if (cmd.hasOption("L")) {
                File subdirectory = new File("DataSet/Large");
                subdirectory.mkdir();
                Writer lout = new FileWriter(new File(subdirectory,"ldata.json"));
                JSONWriter lj = new JSONWriter();
                lj.createDatafiles(lout, Filters.lFilter());

            }
        }
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }
    }
}
