package nz.ac.massey.ghtraffic.scripts.generator;
import java.util.Random;
import org.apache.commons.lang.RandomStringUtils;

/**
 * This class randomly generates hexadecimal strings
 * @author thilini bhagya
 */
public class RandomStringGenerator {

    public static String generateStringValue(int length){

        Random randomService = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length) {
            sb.append(Integer.toHexString(randomService.nextInt()));
        }
        sb.setLength(length);
        return sb.toString();
    }

    //unique hexadecimal string with alphabets in uppercase for github request id
    public static String generateRequestIdValue(){
        Random randomService = new Random();

        //first sub string with length 4
        StringBuilder sb1 = new StringBuilder();
        while (sb1.length() < 4) {
            sb1.append(Integer.toHexString(randomService.nextInt()).toUpperCase());
        }
        sb1.setLength(4);

        //second sub string length 5
        StringBuilder sb2 = new StringBuilder();
        while (sb2.length() < 5) {
            sb2.append(Integer.toHexString(randomService.nextInt()).toUpperCase());
        }
        sb2.setLength(5);

        //third sub string length 7
        StringBuilder sb3 = new StringBuilder();
        while (sb3.length() < 7) {
            sb3.append(Integer.toHexString(randomService.nextInt()).toUpperCase());
        }
        sb3.setLength(7);

        //fourth sub string length 7
        StringBuilder sb4 = new StringBuilder();
        while (sb4.length() < 7) {
            sb4.append(Integer.toHexString(randomService.nextInt()).toUpperCase());
        }
        sb4.setLength(7);

        //fifth sub string length 8
        StringBuilder sb5 = new StringBuilder();
        while (sb5.length() < 8) {
            sb5.append(Integer.toHexString(randomService.nextInt()).toUpperCase());
        }
        sb5.setLength(8);

        System.out.println(sb1.toString()+":"+sb2.toString());
        return sb1.toString()+":"+sb2.toString()+":"+sb3.toString()+":"+sb4.toString()+":"+sb5.toString();
    }

    public static String generateAlphabeticStringValue(){

        String generatedString = RandomStringUtils.randomAlphabetic(5);
        return generatedString;
    }
}
