package nz.ac.massey.ghtraffic.scripts.exporter;
import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.standards.HttpTransaction;
import nz.ac.massey.ghtraffic.scripts.standards.*;
import org.apache.log4j.Logger;
import java.io.*;
import java.util.*;
import org.json.simple.JSONObject;

/**
 * This class converts HTTP transactions to Json object
 * @author thilini bhagya
 */
public class GHTorrentTransactionToJSONExporter implements HttpTransactionExporterFactory {

    static Logger LOGGER = Logging.getLogger(GHTorrentTransactionToJSONExporter.class);

    public CharSequence getExporter(HttpTransaction item) throws IOException {
        return exportToJson(item);
    }

    private CharSequence exportToJson(HttpTransaction item) throws IOException {
        LOGGER.info("Converting transaction to JSON format");

        JSONObject mainObject = new JSONObject();

        JSONObject jsonRequestObject = new JSONObject();
        jsonRequestObject.put("Method", item.getRequest().getMethod().toString());
        jsonRequestObject.put("Request-URI", item.getRequest().getRequestUri());
        jsonRequestObject.put("HTTP-Version", item.getRequest().getHttpVersion());

        JSONObject jsonRequestHeaderObject = new JSONObject();
        for (KeyValuePair header : item.getRequest().getMessageHeader()) {
            jsonRequestHeaderObject.put(header.getKey(), header.getValue());
        }
        jsonRequestObject.put("Message-Header", jsonRequestHeaderObject);

        //convert name-value pairs to a json object to include as a message body
        JSONObject jsonRequestBodyObject = new JSONObject();
        //check whether the request has an entity to convert as json object
        if (item.getRequest().getMessageBody() != null && item.getResponse().getStatusCode() != HttpStatus.BAD_REQUEST.getCode()) {
            for (KeyValuePair entity : item.getRequest().getMessageBody()) {
                //set JSONObject.NULL instead of null values in message body
                if (entity.getValue() == null) {
                    jsonRequestBodyObject.put(entity.getKey(), org.json.JSONObject.NULL);
                }
                //convert String object to Boolean Object
                else if (entity.getValue() == "false") {
                    jsonRequestBodyObject.put(entity.getKey(), Boolean.parseBoolean("false"));
                } else if (entity.getValue() == "true") {
                    jsonRequestBodyObject.put(entity.getKey(), Boolean.parseBoolean("true"));
                } else {
                    jsonRequestBodyObject.put(entity.getKey(), entity.getValue());
                }
                //convert the json object to a json string
                String jsonString = jsonRequestBodyObject.toJSONString();
                //add json message string as the value for Message-Body
                jsonRequestObject.put("Message-Body", jsonString);}
        }
        //form invalid json format for message body of 400 status code
        else if(item.getRequest().getMessageBody() != null && item.getResponse().getStatusCode() == HttpStatus.BAD_REQUEST.getCode()){
            for (KeyValuePair entity : item.getRequest().getMessageBody()) {
                String invalidjsonBody = "{"+"\"" +entity.getKey()+entity.getValue()+ "\"" + "}";
                jsonRequestObject.put("Message-Body", invalidjsonBody);
            }
        }

        mainObject.put("Request", jsonRequestObject);

        JSONObject jsonResponseObject = new JSONObject();
        jsonResponseObject.put("Status-Code", item.getResponse().getStatusCode());
        jsonResponseObject.put("Reason-Phrase", item.getResponse().getReasonPhrase());
        jsonResponseObject.put("HTTP-Version", item.getResponse().getHttpVersion());

        JSONObject jsonResponseHeaderObject = new JSONObject();
        for (KeyValuePair header : item.getResponse().getMessageHeader()) {
            jsonResponseHeaderObject.put(header.getKey(), header.getValue());
        }
        jsonResponseObject.put("Message-Header", jsonResponseHeaderObject);

        //convert name-value pairs to a json object to include as a message body
        JSONObject jsonResponseBodyObject = new JSONObject();
        /*check whether the response has an entity to convert as json object
         *we have assigned an entity for HEAD transactions same as GET to calculate Content-Length header,
         *but it does not convert as an json object since HEAD only output headers
         */
        if (item.getResponse().getMessageBody() != null && !item.getRequest().getMethod().equals(HttpMethod.HEAD)) {
            for (KeyValuePair entity : item.getResponse().getMessageBody()) {
                //set JSONObject.NULL instead of null values in message body
                if (entity.getValue() == null) {
                    jsonResponseBodyObject.put(entity.getKey(), org.json.JSONObject.NULL);
                }
                //convert String object to Boolean Object
                else if (entity.getValue() == "false") {
                    jsonResponseBodyObject.put(entity.getKey(), Boolean.parseBoolean("false"));
                } else if (entity.getValue() == "true") {
                    jsonResponseBodyObject.put(entity.getKey(), Boolean.parseBoolean("true"));
                } else {
                    jsonResponseBodyObject.put(entity.getKey(), entity.getValue());
                }
                //convert the json object to a json string
                String jsonString = jsonResponseBodyObject.toJSONString();
                //add json message string as the value for Message-Body
                jsonResponseObject.put("Message-Body", jsonString);}
        }

        mainObject.put("Response", jsonResponseObject);

        Enumeration e = item.getMetaData().propertyNames();
        JSONObject jsonPropertyObject = new JSONObject();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = item.getMetaData().getProperty(key);
            jsonPropertyObject.put(key, value);
        }
        mainObject.put("Meta-Data", jsonPropertyObject);

        return mainObject.toString();
    }
}







