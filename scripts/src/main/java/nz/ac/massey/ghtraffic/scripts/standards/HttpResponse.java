package nz.ac.massey.ghtraffic.scripts.standards;
/*
 * This class represents HTTP Response message as defined in HTTP/1.1 standard (RFC 2616 )
 * @see <a href="https://www.ietf.org/rfc/rfc2616.txt">RFC2616 (HTTP/1.1)</a> , section 6
 * @author thilini bhagya
 * 
 */
import java.util.List;

public class HttpResponse {
	private int statusCode;
	private String reasonPhrase;
	private List<KeyValuePair> messageHeader;
	private List<KeyValuePair> messageBody;
	private long timeStamp;
	private String httpVersion;

	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getReasonPhrase() {
		return reasonPhrase;
	}

	public int getStatusCode() {
		return statusCode;
	}
	
	public List<KeyValuePair> getMessageHeader(){
		return messageHeader;
	}
	
	public void setMessageHeader(List<KeyValuePair> messageHeader){
		this.messageHeader = messageHeader;
	}
	
	public List<KeyValuePair> getMessageBody(){
		return messageBody;
	}
	
	public void setMessageBody(List<KeyValuePair> messageBody){
		this.messageBody = messageBody;
	}

	public void setHttpVersion(String httpVersion){
		this.httpVersion = httpVersion;
	}

	public String getHttpVersion(){
		return httpVersion;
	}

}
