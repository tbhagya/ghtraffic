package nz.ac.massey.ghtraffic.scripts.standards;

/**
 * This class lists Http methods
 * by referring to the HTTP/1.1 standard (RFC 2616 )
 * @see <a href="https://www.ietf.org/rfc/rfc2616.txt">RFC2616 (HTTP/1.1)</a> , section 5.1.1
 * @author thilini bhagya
 */

public enum HttpMethod {
    OPTION,
    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE,
    TRACE,
    CONNECT
}
