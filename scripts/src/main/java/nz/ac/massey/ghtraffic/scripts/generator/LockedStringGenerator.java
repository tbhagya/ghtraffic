package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates string for locked GitHub parameter
 * @author thilini bhagya
 */
public class LockedStringGenerator {
        public static final String TRUE = "true";
        public static final String FALSE = "false";

}
