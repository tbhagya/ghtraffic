package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates strings for X-OAuth-Scopes GitHub header
 * @author thilini bhagya
 */
public class OAuthScopesStringGenerator {
    public static final String PUBLIC = "public_repo";
    public static final String PRIVATE = "private_repo";
    public static final String ANY = "repo";
}
