package nz.ac.massey.ghtraffic.scripts.extractor;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.standards.*;
import nz.ac.massey.ghtraffic.scripts.generator.*;
import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import static nz.ac.massey.ghtraffic.scripts.standards.HttpDateFormatter.getCurrentDate;

/**
 * This class generates successful POST requests to create individual issues
 * @author thilini bhagya
 */

public class GHTorrentTransactionFactoryForIssueCreation implements HttpTransactionFactory {

    static Logger LOGGER = Logging.getLogger(GHTorrentTransactionFactoryForIssueCreation.class);
    private DB mongoDb;
    private DBCursor cursor;

    public Iterator<HttpTransaction> getTransactions() {
        //read from properties file
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            LOGGER.info("Instantiating a new connection to MongoDB");
            Mongo mongo = new Mongo(prop.getProperty("host"), Integer.parseInt(prop.getProperty("port")));
            mongoDb = mongo.getDB(prop.getProperty("database"));
            cursor = mongoDb.getCollection(prop.getProperty("collection")).find();}
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }

        return new Iterator() {
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public HttpTransaction next() {
                DBObject dbObj = cursor.next();
                HttpTransaction transaction = null;
                try {
                    LOGGER.info("Building a transaction for DB object: " + dbObj.get("id"));
                    transaction = build(dbObj);
                } catch (Exception x) {
                    LOGGER.warn("Exception writing details to log ", x);
                    cursor.close();
                }
                return transaction;
            }

            @Override
            public boolean hasNext() {
                boolean hasNext = false;
                try {
                    hasNext = cursor.hasNext();
                }
                catch (Exception x) {
                    LOGGER.warn("Exception writing details to log " ,x);

                }
                return hasNext;
            }
        };
    }
    private HttpTransaction build(DBObject dbObj) throws ParseException, URISyntaxException {
        HttpTransaction transaction = new HttpTransaction();

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();

        //request line (method, url and protocol version)
        request.setMethod(HttpMethod.POST);
        //uri for POST
        URI urlAsString = new URI((String) dbObj.get("url"));
        String uri = (urlAsString.getRawPath()).substring(0, (urlAsString.getRawPath()).lastIndexOf("/"));
        request.setRequestUri(uri);
        //http version
        request.setHttpVersion(HttpVersion.HTTP_1_1);

        //request body
        List<KeyValuePair> requestBody = new ArrayList<KeyValuePair>();
        if (dbObj.containsField(GitHubParameters.TITLE))requestBody.add(new KeyValuePair(GitHubParameters.TITLE, dbObj.get(GitHubParameters.TITLE)));
        if (dbObj.containsField(GitHubParameters.LABELS))
        {
            List<Object> valSetLabels = new ArrayList<Object>();
            List<DBObject> labels = (List<DBObject>) dbObj.get(GitHubParameters.LABELS);
            for (DBObject label : labels) {
                valSetLabels.add(label.get("name"));
            }
            if(!valSetLabels.isEmpty()){
                requestBody.add(new KeyValuePair(GitHubParameters.LABELS, valSetLabels));
            }
        }

        if (dbObj.containsField(GitHubParameters.ASSIGNEE))
        {
            if((DBObject) dbObj.get(GitHubParameters.ASSIGNEE) != null)
            {
                List<DBObject> valSetAssignees = new ArrayList<DBObject>();
                valSetAssignees.add((DBObject)dbObj.get(GitHubParameters.ASSIGNEE));
                if(!valSetAssignees.isEmpty()){
                    requestBody.add(new KeyValuePair(GitHubParameters.ASSIGNEE, ((DBObject)dbObj.get(GitHubParameters.ASSIGNEE)).get("login")));
                    requestBody.add(new KeyValuePair(GitHubParameters.ASSIGNEES, valSetAssignees));
                }
            }
        }

        if (dbObj.containsField(GitHubParameters.MILESTONE))
        {
            if(dbObj.get(GitHubParameters.MILESTONE) != null)
            {
                requestBody.add(new KeyValuePair(GitHubParameters.MILESTONE, ((DBObject) dbObj.get(GitHubParameters.MILESTONE)).get("number")));
            }
        }
        if (dbObj.containsField(GitHubParameters.BODY))requestBody.add(new KeyValuePair(GitHubParameters.BODY, dbObj.get(GitHubParameters.BODY)));

        request.setMessageBody(requestBody);

        //request headers
        Random random = new Random();
        int index = random.nextInt(UserAgentStringGenerator.agents.size());
        List<KeyValuePair> requestHeaders = new ArrayList<KeyValuePair>();
        requestHeaders.add(new KeyValuePair(HttpHeaders.HOST, HostStringGenerator.HOST));
        requestHeaders.add(new KeyValuePair(HttpHeaders.USER_AGENT, UserAgentStringGenerator.agents.get(index)));
        requestHeaders.add(new KeyValuePair(HttpHeaders.ACCEPT, AcceptStringGenerator.anyMIMEtype));
        requestHeaders.add(new KeyValuePair(HttpHeaders.AUTHORIZATION, "token "+RandomStringGenerator.generateStringValue(40)));
        requestHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_TYPE, ContentTypeStringGenerator.JSON));
        requestHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_LENGTH, ContentLengthGenerator.count(request.getMessageBody().toString())));
        request.setMessageHeader(requestHeaders);

        //response line
        response.setStatusCode(HttpStatus.CREATED.getCode());
        response.setReasonPhrase(HttpStatus.CREATED.getReasonPhrase());
        response.setHttpVersion(HttpVersion.HTTP_1_1);

        //response body
        List<KeyValuePair> responseBody = new ArrayList<KeyValuePair>();
        if (dbObj.containsField(GitHubParameters.URL))responseBody.add(new KeyValuePair(GitHubParameters.URL, dbObj.get(GitHubParameters.URL)));
        if (dbObj.containsField(GitHubParameters.LABELS_URL))responseBody.add(new KeyValuePair(GitHubParameters.LABELS_URL, dbObj.get(GitHubParameters.LABELS_URL)));
        if (dbObj.containsField(GitHubParameters.HTML_URL))responseBody.add(new KeyValuePair(GitHubParameters.HTML_URL, dbObj.get(GitHubParameters.HTML_URL)));
        if (dbObj.containsField(GitHubParameters.ID))responseBody.add(new KeyValuePair(GitHubParameters.ID, dbObj.get(GitHubParameters.ID)));
        if (dbObj.containsField(GitHubParameters.NUMBER))responseBody.add(new KeyValuePair(GitHubParameters.NUMBER, dbObj.get(GitHubParameters.NUMBER)));
        if (dbObj.containsField(GitHubParameters.TITLE))responseBody.add(new KeyValuePair(GitHubParameters.TITLE, dbObj.get(GitHubParameters.TITLE)));
        if (dbObj.containsField(GitHubParameters.USER))responseBody.add(new KeyValuePair(GitHubParameters.USER, dbObj.get(GitHubParameters.USER)));
        if (dbObj.containsField(GitHubParameters.LABELS))responseBody.add(new KeyValuePair(GitHubParameters.LABELS, dbObj.get(GitHubParameters.LABELS)));
        if (dbObj.containsField(GitHubParameters.STATE))responseBody.add(new KeyValuePair(GitHubParameters.STATE, StateStringGenerator.OPEN));
        if (dbObj.containsField(GitHubParameters.LOCKED))responseBody.add(new KeyValuePair(GitHubParameters.LOCKED, LockedStringGenerator.FALSE));
        if (dbObj.containsField(GitHubParameters.ASSIGNEE))
        {
            if((DBObject) dbObj.get(GitHubParameters.ASSIGNEE) != null)
            {
                responseBody.add(new KeyValuePair(GitHubParameters.ASSIGNEE, ((DBObject) dbObj.get(GitHubParameters.ASSIGNEE))));
                List<DBObject> valSetAssignees = new ArrayList<DBObject>();
                valSetAssignees.add((DBObject) dbObj.get(GitHubParameters.ASSIGNEE));
                responseBody.add(new KeyValuePair(GitHubParameters.ASSIGNEES, valSetAssignees));
            }
            else{
                responseBody.add(new KeyValuePair(GitHubParameters.ASSIGNEE,dbObj.get(GitHubParameters.ASSIGNEE)));
                List<DBObject> valSetAssignees = new ArrayList<DBObject>();
                responseBody.add(new KeyValuePair(GitHubParameters.ASSIGNEES,valSetAssignees));
            }
        }
        if (dbObj.containsField(GitHubParameters.MILESTONE))responseBody.add(new KeyValuePair(GitHubParameters.MILESTONE, dbObj.get(GitHubParameters.MILESTONE)));
        if (dbObj.containsField(GitHubParameters.COMMENTS))responseBody.add(new KeyValuePair(GitHubParameters.COMMENTS, 0));
        if (dbObj.containsField(GitHubParameters.CREATED_AT))responseBody.add(new KeyValuePair(GitHubParameters.CREATED_AT, dbObj.get(GitHubParameters.CREATED_AT)));
        if (dbObj.containsField(GitHubParameters.UPDATED_AT))responseBody.add(new KeyValuePair(GitHubParameters.UPDATED_AT, dbObj.get(GitHubParameters.CREATED_AT)));
        if (dbObj.containsField(GitHubParameters.CLOSED_AT))responseBody.add(new KeyValuePair(GitHubParameters.CLOSED_AT, null));
        if (dbObj.containsField(GitHubParameters.BODY))responseBody.add(new KeyValuePair(GitHubParameters.BODY, dbObj.get(GitHubParameters.BODY)));
        if (dbObj.containsField(GitHubParameters.CLOSED_BY)) responseBody.add(new KeyValuePair(GitHubParameters.CLOSED_BY, null));
        response.setMessageBody(responseBody);

        //response headers
        List<KeyValuePair> responseHeaders = new ArrayList<KeyValuePair>();
        if (dbObj.containsField(GitHubParameters.CREATED_AT)) responseHeaders.add(new KeyValuePair(HttpHeaders.DATE, HttpDateFormatter.getDate((String) dbObj.get(GitHubParameters.CREATED_AT))));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_TYPE, ContentTypeStringGenerator.JSON));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_LENGTH, ContentLengthGenerator.count(response.getMessageBody().toString())));
        responseHeaders.add(new KeyValuePair(HttpHeaders.SERVER, ServerStringGenerator.GITHUB));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ETAG, RandomStringGenerator.generateStringValue(32)));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CACHE_CONTROL, CacheControlStringGenerator.PRIVATE));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_OAUTH_SCOPES, OAuthScopesStringGenerator.PUBLIC));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES, OAuthScopesStringGenerator.PUBLIC+", "+OAuthScopesStringGenerator.ANY));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_MEDIA_TYPE, ContentTypeStringGenerator.GITHUB_MEDIA_TYPE));
        responseHeaders.add(new KeyValuePair(HttpHeaders.VARY, HttpHeaders.ACCEPT+", "+HttpHeaders.AUTHORIZATION+", "+HttpHeaders.COOKIE));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, OriginStringGenerator.ANY_ORIGIN));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG+", "+GitHubCustomHeaders.X_OAUTH_SCOPES+", "+GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_REQUEST_ID, RandomStringGenerator.generateRequestIdValue()));
        if (dbObj.containsField(GitHubParameters.URL)) responseHeaders.add(new KeyValuePair(HttpHeaders.LOCATION, dbObj.get(GitHubParameters.URL)));
        response.setMessageHeader(responseHeaders);

        //properties
        Properties prop = new Properties();
        prop.setProperty(GHTrafficProperties.TYPE, TypeStringGenerator.REAL_WORLD);
        prop.setProperty(GHTrafficProperties.SOURCE, SourceStringGenerator.GHTORRENT);
        prop.setProperty(GHTrafficProperties.PROCESSOR, this.getClass().getName());
        prop.setProperty(GHTrafficProperties.TIMESTAMP, getCurrentDate());
        transaction.setMetaData(prop);

        transaction.setRequest(request);
        transaction.setResponse(response);

        return transaction;
    }
}







