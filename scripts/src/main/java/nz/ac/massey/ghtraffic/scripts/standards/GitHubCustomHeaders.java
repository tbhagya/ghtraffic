package nz.ac.massey.ghtraffic.scripts.standards;

/*
 * This class lists custom headers for GitHub v3 API
 * @see <a href="https://developer.github.com/v3/">https://developer.github.com/v3/</a>
 * @author thilini bhagya
 */
public interface GitHubCustomHeaders {
    public static final String X_GITHUB_MEDIA_TYPE = "X-GitHub-Media-Type";
    public static final String X_RATELIMIT_LIMIT = "X-RateLimit-Limit";
    public static final String X_RATELIMIT_REMAINING = "X-RateLimit-Remaining";
    public static final String X_RATELIMIT_RESET = "X-RateLimit-Reset";
    public static final String X_CONTENT_TYPE_OPTION = "X-Content-Type-Options";
    public static final String X_OAUTH_SCOPES = "X-OAuth-Scopes";
    public static final String X_ACCEPTED_OAUTH_SCOPES = "X-Accepted-OAuth-Scopes";
    public static final String X_FRAME_OPTIONS = "X-Frame-Options";
    public static final String X_GITHUB_REQUEST_ID = "X-GitHub-Request-Id";
    public static final String X_RUNTIME_RACK = "X-Runtime-Rack";
    public static final String X_XSS_PROTECTION = "X-XSS-Protection";


}
