package nz.ac.massey.ghtraffic.scripts.standards;

/*
 * This class represents Http Request message as defined in HTTP/1.1 standard (RFC 2616 )
 * @see <a href="https://www.ietf.org/rfc/rfc2616.txt">RFC2616 (HTTP/1.1)</a> , section 5
 * @author thilini bhagya
 * 
 */
import java.util.*;

public class HttpRequest{
	
	HttpMethod method;
	private String requestUri;
	private List<KeyValuePair> messageHeader;
    private List<KeyValuePair> messageBody;
	private long timeStamp;
	private String httpVersion;

	public HttpMethod getMethod(){
		return method;
	}
	
	public void setMethod(HttpMethod method){
		this.method = method;
	}
	
	public String getRequestUri(){
		return requestUri;
	}
	
	public void setRequestUri(String uri){
		this.requestUri = uri;
	}
	
	public List<KeyValuePair> getMessageHeader(){
		return messageHeader;
	}
	
	public void setMessageHeader(List<KeyValuePair> messageHeader){
		this.messageHeader = messageHeader;
	}
	
	public List<KeyValuePair> getMessageBody(){
		return messageBody;
}

	public void setMessageBody(List<KeyValuePair> messageBody){
		this.messageBody = messageBody;
	}

	public void setHttpVersion(String httpVersion){
		this.httpVersion = httpVersion;
	}

	public String getHttpVersion(){
		return httpVersion;
	}

}
