package nz.ac.massey.ghtraffic.scripts.exporter;
import nz.ac.massey.ghtraffic.scripts.standards.HttpTransaction;
import java.io.IOException;

/**
 * Factory for exporting Http transactions to different formats
 * @author thilini bhagya
 */

public interface HttpTransactionExporterFactory {
    CharSequence getExporter(HttpTransaction item) throws IOException;
}
