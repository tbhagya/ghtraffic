package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates strings for Accept Http header
 * @author thilini bhagya
 */
public class AcceptStringGenerator {
    public static final String anyMIMEtype = "*/*";
}
