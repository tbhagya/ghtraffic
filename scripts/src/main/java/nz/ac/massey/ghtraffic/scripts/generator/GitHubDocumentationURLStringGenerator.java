package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates string values for documentation_url parameter for errors on API calls
 * @author thilini bhagya
 */
public class GitHubDocumentationURLStringGenerator {
    public static final String GENERAL_URL = "https://developer.github.com/v3";
    public static final String SPECIFIC_URL_ISSUE_UNLOCK = "https://developer.github.com/v3/issues/#unlock-an-issue";
    public static final String SPECIFIC_URL_ISSUE_CREATION = "https://developer.github.com/v3/issues/#create-an-issue";
    public static final String SPECIFIC_URL_ISSUE_EDITING = "https://developer.github.com/v3/issues/#edit-an-issue";
    public static final String SPECIFIC_URL_ISSUE_LOCK = "https://developer.github.com/v3/issues/#lock-an-issue";
}
