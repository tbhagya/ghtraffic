package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class counts content length for Content-Length Http header
 * @author thilini bhagya
 */
public class ContentLengthGenerator {

    public static int count(String entity) {
        int counter = 0;
        for (int i = 0; i < entity.length(); i++) {
            if (Character.isLetterOrDigit(entity.charAt(i))||Character.isSpaceChar(entity.charAt(i)))
                counter++;
        }
        return counter;
    }
}
