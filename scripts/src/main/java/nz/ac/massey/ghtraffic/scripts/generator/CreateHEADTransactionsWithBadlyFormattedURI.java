package nz.ac.massey.ghtraffic.scripts.generator;

import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.extractor.HttpTransactionFactory;
import nz.ac.massey.ghtraffic.scripts.standards.*;
import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.*;
import static nz.ac.massey.ghtraffic.scripts.standards.HttpDateFormatter.getCurrentDate;

/**
 * This class generates HEAD transactions with badly formatted URL
 * @author thilini bhagya
 */
public class CreateHEADTransactionsWithBadlyFormattedURI implements HttpTransactionFactory {
    private DB mongoDb;
    private DBCursor cursor;
    static Logger LOGGER = Logging.getLogger(CreateHEADTransactionsWithBadlyFormattedURI.class);

    public Iterator<HttpTransaction> getTransactions() {

        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("config.properties"));
            LOGGER.info("Instantiating a new connection to MongoDB");
            Mongo mongo = new Mongo(prop.getProperty("host"), Integer.parseInt(prop.getProperty("port")));
            mongoDb = mongo.getDB(prop.getProperty("database"));
            //get 40% from total size
            int count = (int) (mongoDb.getCollection(prop.getProperty("collection")).find().size() * (40.0f / 100.0f));
            //generate a random number within 40%
            int randomNumber = (int) Math.floor(Math.random() * count);
            //generate 40% transactions by skipping the number of records
            cursor = mongoDb.getCollection(prop.getProperty("collection")).find().skip(randomNumber).limit(count);
        }
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }

        return new Iterator() {
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public HttpTransaction next() {
                DBObject dbObj = cursor.next();
                HttpTransaction transaction = null;
                try {
                    LOGGER.info("Building a transaction for DB object: "+dbObj.get("id"));
                    transaction = build(dbObj);
                } catch (Exception x) {
                    LOGGER.warn("Exception writing details to log " ,x);
                    cursor.close();
                }
                return transaction;
            }
            @Override
            public boolean hasNext() {
                boolean hasNext = false;
                try {
                    hasNext = cursor.hasNext();
                }
                catch (Exception x) {
                    LOGGER.warn("Exception writing details to log " ,x);

                }
                return hasNext;
            }
        };
    }

    private HttpTransaction build(DBObject dbObj) throws ParseException, URISyntaxException {
        HttpTransaction transaction = new HttpTransaction();

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();

        //request line (method, url and protocol version)
        request.setMethod(HttpMethod.HEAD);
        /*invalid uri
         *added a random string at the end of each repository name
         */
        URI urlAsString = new URI((String) dbObj.get(GitHubParameters.URL));
        String uri = urlAsString.getRawPath().substring(0,(urlAsString.getRawPath()).indexOf("/issues"));
        request.setRequestUri(uri+"/"+RandomStringGenerator.generateAlphabeticStringValue()+"/issues/"+Integer.toString((int)dbObj.get(GitHubParameters.NUMBER)));
        //http version
        request.setHttpVersion(HttpVersion.HTTP_1_1);

        // request headers
        Random random = new Random();
        int index = random.nextInt(UserAgentStringGenerator.agents.size());
        List<KeyValuePair> requestHeaders = new ArrayList<KeyValuePair>();
        requestHeaders.add(new KeyValuePair(HttpHeaders.HOST, HostStringGenerator.HOST));
        requestHeaders.add(new KeyValuePair(HttpHeaders.USER_AGENT, UserAgentStringGenerator.agents.get(index)));
        requestHeaders.add(new KeyValuePair(HttpHeaders.ACCEPT, AcceptStringGenerator.anyMIMEtype));
        requestHeaders.add(new KeyValuePair(HttpHeaders.AUTHORIZATION, RandomStringGenerator.generateStringValue(40)));
        request.setMessageHeader(requestHeaders);

        //response line
        response.setStatusCode(HttpStatus.NOT_FOUND.getCode());
        response.setReasonPhrase(HttpStatus.NOT_FOUND.getReasonPhrase());
        response.setHttpVersion(HttpVersion.HTTP_1_1);

        //set response body as same as in CreateGETTransactionsWithBadlyFormattedURI but it not converts to json
        List <KeyValuePair> responseBody = new ArrayList <KeyValuePair>();
        responseBody.add(new KeyValuePair(GitHubParameters.MESSAGE, HttpStatus.NOT_FOUND.getReasonPhrase()));
        responseBody.add(new KeyValuePair(GitHubParameters.DOCUMENTATION_URL, GitHubDocumentationURLStringGenerator.GENERAL_URL));
        response.setMessageBody(responseBody);

        //response headers
        List<KeyValuePair> responseHeaders = new ArrayList<KeyValuePair>();
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_TYPE, ContentTypeStringGenerator.JSON));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_LENGTH, ContentLengthGenerator.count(response.getMessageBody().toString())));
        responseHeaders.add(new KeyValuePair(HttpHeaders.SERVER, ServerStringGenerator.GITHUB));
        responseHeaders.add(new KeyValuePair(HttpHeaders.DATE, HttpDateFormatter.getCurrentDate()));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_OAUTH_SCOPES, OAuthScopesStringGenerator.PUBLIC));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES, OAuthScopesStringGenerator.ANY));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_MEDIA_TYPE, ContentTypeStringGenerator.GITHUB_MEDIA_TYPE));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, OriginStringGenerator.ANY_ORIGIN));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG+", "+GitHubCustomHeaders.X_OAUTH_SCOPES+", "+GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_REQUEST_ID, RandomStringGenerator.generateRequestIdValue()));
        response.setMessageHeader(responseHeaders);

        //properties
        Properties prop = new Properties();
        prop.setProperty(GHTrafficProperties.TYPE, TypeStringGenerator.SYNTHETIC);
        prop.setProperty(GHTrafficProperties.SOURCE, SourceStringGenerator.GHTORRENT);
        prop.setProperty(GHTrafficProperties.PROCESSOR, this.getClass().getName());
        prop.setProperty(GHTrafficProperties.TIMESTAMP, getCurrentDate());
        transaction.setMetaData(prop);

        transaction.setRequest(request);
        transaction.setResponse(response);


        return transaction;
    }
}
