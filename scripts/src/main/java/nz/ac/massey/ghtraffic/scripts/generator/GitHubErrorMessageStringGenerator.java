package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates strings for error messages
 * @author thilini bhagya
 */
public class GitHubErrorMessageStringGenerator {
    public static final String WITHOUT_REQUEST_BODY = "Invalid request.\\n\\nFor 'links/0/schema', nil is not an object.";
    public static final String WITHOUT_AUTHENTICATION = "Requires authentication";
    public static final String INVALID_JSON = "Problems parsing JSON";
}

