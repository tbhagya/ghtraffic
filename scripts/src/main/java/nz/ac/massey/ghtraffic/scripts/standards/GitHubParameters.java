package nz.ac.massey.ghtraffic.scripts.standards;

/**
 * This class lists parameter names for request and response bodies on Issues
 * @see <a href="https://developer.github.com/v3/issues/">Issues API</a>
 * @author thilini bhagya
 */
public interface GitHubParameters {

    public static final String TITLE = "title";
    public static final String URL = "url";
    public static final String REPOSITORY_URL = "repository_url";
    public static final String LABELS_URL = "labels_url";
    public static final String COMMENTS_URL = "comments_url";
    public static final String EVENTS_URL = "events_url";
    public static final String HTML_URL = "html_url";
    public static final String ID = "id";
    public static final String NUMBER = "number";
    public static final String USER = "user";
    public static final String LABELS = "labels";
    public static final String STATE = "state";
    public static final String LOCKED = "locked";
    public static final String ASSIGNEE = "assignee";
    public static final String ASSIGNEES = "assignees";
    public static final String MILESTONE = "milestone";
    public static final String COMMENTS = "comments";
    public static final String CREATED_AT = "created_at";
    public static final String CLOSED_AT = "closed_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String BODY = "body";
    public static final String CLOSED_BY = "closed_by";

    //for response bodies of errors on API calls
    public static final String MESSAGE = "message";
    public static final String DOCUMENTATION_URL = "documentation_url";
}
