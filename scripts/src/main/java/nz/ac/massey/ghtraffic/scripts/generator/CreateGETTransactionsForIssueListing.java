package nz.ac.massey.ghtraffic.scripts.generator;

import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.standards.*;
import nz.ac.massey.ghtraffic.scripts.extractor.HttpTransactionFactory;
import static nz.ac.massey.ghtraffic.scripts.standards.HttpDateFormatter.*;
import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

/**
 * This class generates successful GET requests to query for individual issues
 * @author thilini bhagya
 */
public class CreateGETTransactionsForIssueListing implements HttpTransactionFactory {

    private DB mongoDb;
    private DBCursor cursor;
    static Logger LOGGER = Logging.getLogger(CreateGETTransactionsForIssueListing.class);

    public Iterator<HttpTransaction> getTransactions() {

        Properties prop = new Properties();
        try{
            prop.load(new FileInputStream("config.properties"));
            LOGGER.info("Instantiating a new connection to MongoDB");
            Mongo mongo = new Mongo(prop.getProperty("host"), Integer.parseInt(prop.getProperty("port")));
            mongoDb = mongo.getDB(prop.getProperty("database"));
            cursor = mongoDb.getCollection(prop.getProperty("collection")).find();

        }
        catch (Exception x) {
            LOGGER.warn("Exception writing details to log " ,x);
        }
        return new Iterator() {
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public HttpTransaction next() {
                DBObject dbObj = cursor.next();
                HttpTransaction transaction = null;
                try {
                    LOGGER.info("Building a transaction for DB object: "+dbObj.get("id"));
                    transaction = build(dbObj);
                } catch (Exception x) {
                    cursor.close();
                    LOGGER.warn("Exception writing details to log " ,x);
                }
                return transaction;
            }

            @Override
            public boolean hasNext() {
                boolean hasNext = false;
                try {
                    hasNext = cursor.hasNext();
                }
                catch (Exception x) {
                    LOGGER.warn("Exception writing details to log " ,x);

                }
                return hasNext;
            }
        };
    }

    private HttpTransaction build(DBObject dbObj) throws ParseException, URISyntaxException {
        HttpTransaction transaction = new HttpTransaction();

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();

        //request line (method, url and protocol version)
        request.setMethod(HttpMethod.GET);
        //uri for GET
        URI urlAsString = new URI((String) dbObj.get(GitHubParameters.URL));
        request.setRequestUri((urlAsString.getRawPath()));
        //http version
        request.setHttpVersion(HttpVersion.HTTP_1_1);

        //request headers
        Random random = new Random();
        int index = random.nextInt(UserAgentStringGenerator.agents.size());
        List<KeyValuePair> requestHeaders = new ArrayList<KeyValuePair>();
        requestHeaders.add(new KeyValuePair(HttpHeaders.HOST, HostStringGenerator.HOST));
        requestHeaders.add(new KeyValuePair(HttpHeaders.USER_AGENT, UserAgentStringGenerator.agents.get(index)));
        requestHeaders.add(new KeyValuePair(HttpHeaders.ACCEPT, AcceptStringGenerator.anyMIMEtype));
        requestHeaders.add(new KeyValuePair(HttpHeaders.AUTHORIZATION, "token "+RandomStringGenerator.generateStringValue(40)));
        request.setMessageHeader(requestHeaders);

        //response line
        response.setStatusCode(HttpStatus.OK.getCode());
        response.setReasonPhrase(HttpStatus.OK.getReasonPhrase());
        response.setHttpVersion(HttpVersion.HTTP_1_1);

        //response body
        List<KeyValuePair> responseBody = new ArrayList<KeyValuePair>();
        if (dbObj.containsField(GitHubParameters.URL))responseBody.add(new KeyValuePair(GitHubParameters.URL, dbObj.get(GitHubParameters.URL)));
        if (dbObj.containsField(GitHubParameters.LABELS_URL))responseBody.add(new KeyValuePair(GitHubParameters.LABELS_URL, dbObj.get(GitHubParameters.LABELS_URL)));
        if (dbObj.containsField(GitHubParameters.HTML_URL))responseBody.add(new KeyValuePair(GitHubParameters.HTML_URL, dbObj.get(GitHubParameters.HTML_URL)));
        if (dbObj.containsField(GitHubParameters.ID))responseBody.add(new KeyValuePair(GitHubParameters.ID, dbObj.get(GitHubParameters.ID)));
        if (dbObj.containsField(GitHubParameters.NUMBER))responseBody.add(new KeyValuePair(GitHubParameters.NUMBER, dbObj.get(GitHubParameters.NUMBER)));
        if (dbObj.containsField(GitHubParameters.TITLE))responseBody.add(new KeyValuePair(GitHubParameters.TITLE, dbObj.get(GitHubParameters.TITLE)));
        if (dbObj.containsField(GitHubParameters.USER))responseBody.add(new KeyValuePair(GitHubParameters.USER, dbObj.get(GitHubParameters.USER)));
        if (dbObj.containsField(GitHubParameters.LABELS))responseBody.add(new KeyValuePair(GitHubParameters.LABELS, dbObj.get(GitHubParameters.LABELS)));
        if (dbObj.containsField(GitHubParameters.STATE))responseBody.add(new KeyValuePair(GitHubParameters.STATE, dbObj.get(GitHubParameters.STATE)));
        if (dbObj.containsField(GitHubParameters.LOCKED))responseBody.add(new KeyValuePair(GitHubParameters.LOCKED, dbObj.get(GitHubParameters.LOCKED)));
        if (dbObj.containsField(GitHubParameters.ASSIGNEE))responseBody.add(new KeyValuePair(GitHubParameters.ASSIGNEE, dbObj.get(GitHubParameters.ASSIGNEE)));
        if (dbObj.containsField(GitHubParameters.MILESTONE))responseBody.add(new KeyValuePair(GitHubParameters.MILESTONE, dbObj.get(GitHubParameters.MILESTONE)));
        if (dbObj.containsField(GitHubParameters.COMMENTS))responseBody.add(new KeyValuePair(GitHubParameters.COMMENTS, dbObj.get(GitHubParameters.COMMENTS)));
        if (dbObj.containsField(GitHubParameters.CREATED_AT))responseBody.add(new KeyValuePair(GitHubParameters.CREATED_AT, dbObj.get(GitHubParameters.CREATED_AT)));
        if (dbObj.containsField(GitHubParameters.UPDATED_AT))responseBody.add(new KeyValuePair(GitHubParameters.UPDATED_AT, dbObj.get(GitHubParameters.UPDATED_AT)));
        if (dbObj.containsField(GitHubParameters.CLOSED_AT))responseBody.add(new KeyValuePair(GitHubParameters.CLOSED_AT, dbObj.get(GitHubParameters.CLOSED_AT)));
        if (dbObj.containsField(GitHubParameters.BODY))responseBody.add(new KeyValuePair(GitHubParameters.BODY, dbObj.get(GitHubParameters.BODY)));
        if (dbObj.containsField(GitHubParameters.CLOSED_BY)) responseBody.add(new KeyValuePair(GitHubParameters.CLOSED_BY, dbObj.get(GitHubParameters.CLOSED_BY)));
        response.setMessageBody(responseBody);

        //response headers
        List<KeyValuePair> responseHeaders = new ArrayList<KeyValuePair>();
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_TYPE, ContentTypeStringGenerator.JSON));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CONTENT_LENGTH, ContentLengthGenerator.count(response.getMessageBody().toString())));
        responseHeaders.add(new KeyValuePair(HttpHeaders.SERVER, ServerStringGenerator.GITHUB));
        responseHeaders.add(new KeyValuePair(HttpHeaders.DATE, HttpDateFormatter.getCurrentDate()));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ETAG, RandomStringGenerator.generateStringValue(32)));
        if (dbObj.containsField(GitHubParameters.UPDATED_AT)) responseHeaders.add(new KeyValuePair(HttpHeaders.LAST_MODIFIED, HttpDateFormatter.getDate((String) dbObj.get(GitHubParameters.UPDATED_AT))));
        responseHeaders.add(new KeyValuePair(HttpHeaders.CACHE_CONTROL, CacheControlStringGenerator.PRIVATE));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_OAUTH_SCOPES, OAuthScopesStringGenerator.PUBLIC));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES, OAuthScopesStringGenerator.PUBLIC+", "+OAuthScopesStringGenerator.ANY));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_MEDIA_TYPE, ContentTypeStringGenerator.GITHUB_MEDIA_TYPE));
        responseHeaders.add(new KeyValuePair(HttpHeaders.VARY, HttpHeaders.ACCEPT+", "+HttpHeaders.AUTHORIZATION+", "+HttpHeaders.COOKIE));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, OriginStringGenerator.ANY_ORIGIN));
        responseHeaders.add(new KeyValuePair(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.ETAG+", "+GitHubCustomHeaders.X_OAUTH_SCOPES+", "+GitHubCustomHeaders.X_ACCEPTED_OAUTH_SCOPES));
        responseHeaders.add(new KeyValuePair(GitHubCustomHeaders.X_GITHUB_REQUEST_ID, RandomStringGenerator.generateRequestIdValue()));
        response.setMessageHeader(responseHeaders);

        //properties
        Properties prop = new Properties();
        prop.setProperty(GHTrafficProperties.TYPE, TypeStringGenerator.SYNTHETIC);
        prop.setProperty(GHTrafficProperties.SOURCE, SourceStringGenerator.GHTORRENT);
        prop.setProperty(GHTrafficProperties.PROCESSOR, this.getClass().getName());
        prop.setProperty(GHTrafficProperties.TIMESTAMP, getCurrentDate());
        transaction.setMetaData(prop);

        transaction.setRequest(request);
        transaction.setResponse(response);

        return transaction;

    }
}