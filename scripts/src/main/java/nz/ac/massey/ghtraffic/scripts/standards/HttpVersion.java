package nz.ac.massey.ghtraffic.scripts.standards;

/**
 * This class lists versions of the Http protocol
 * @author thilini bhagya
 */
public interface HttpVersion {
    public static final String HTTP_0_9 = "HTTP/0.9";
    public static final String HTTP_1_0 = "HTTP/1.0";
    public static final String HTTP_1_1 = "HTTP/1.1";
    public static final String HTTP_2 = "HTTP/2";
}
