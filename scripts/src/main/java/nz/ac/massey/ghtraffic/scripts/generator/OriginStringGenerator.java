package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * This class generates strings for Access-Control-Allow-Origin Http header
 * @author thilini bhagya
 */

public class OriginStringGenerator {
    public static final String ANY_ORIGIN = "*";
}
