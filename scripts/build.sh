#!/bin/bash 
mvn clean install
mvn -q clean compile  exec:java -Dexec.mainClass="nz.ac.massey.ghtraffic.scripts.generator.Main" -Dexec.args="-$*"
exit 0