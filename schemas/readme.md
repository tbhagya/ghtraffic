## Using the Schema Validation Tool

- cd into **validator** folder

- Run **validation.sh** by passing JSON content file path as first argument and JSON schema file path as second argument

  > Following command validates the content of **post-record.json** with **post.schema.json** schema
  >
  > ./validate.sh ~/ghtraffic/schemas/post.schema.json ~/ghtraffic/schemas/sample-records/post-record.json 

- The result is a message stating the content is valid or not


