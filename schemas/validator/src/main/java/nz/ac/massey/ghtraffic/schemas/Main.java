package nz.ac.massey.ghtraffic.schemas;

/**
 *  Load the schema and the JSON documents from user provided location
 *  and check validity
 *  @author thilini bhagya
 */

import java.io.File;

public class Main {
    public static void main( String[] args ) throws Exception
    {
        String schemaFilePath = args[0];
        String jsonFilePath = args[1];

        File schemaFile = new File(schemaFilePath);
        File jsonFile = new File(jsonFilePath);

        if (ValidationUtils.isJsonValid(schemaFile, jsonFile)){
            System.out.println("Valid");
        }else{
            System.out.println("NOT Valid");
        }

    }
}