package nz.ac.massey.ghtraffic.schemas;

import org.junit.Test;
import java.io.File;
import static org.junit.Assert.*;

/**
 * checks whether the json schemas are matched with different GHTraffic records
 * @author thilini bhagya
 */
public class TestMain {
    @Test
    public final void postJsonSchemaIsValidWithPostRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record1 = new File(classLoader.getResource("records/post-record1.json").getFile());
        File record2 = new File(classLoader.getResource("records/post-record2.json").getFile());
        File record3 = new File(classLoader.getResource("records/post-record3.json").getFile());
        File record4 = new File(classLoader.getResource("records/post-record4.json").getFile());
        File schema = new File(classLoader.getResource("schemas/post.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record1));
        assertTrue(ValidationUtils.isJsonValid(schema,record2));
        assertTrue(ValidationUtils.isJsonValid(schema,record3));
        assertTrue(ValidationUtils.isJsonValid(schema,record4));
    }

    @Test
    public final void patchJsonSchemaIsValidWithPatchRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record1 = new File(classLoader.getResource("records/patch-record1.json").getFile());
        File record2 = new File(classLoader.getResource("records/patch-record2.json").getFile());
        File schema = new File(classLoader.getResource("schemas/patch.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record1));
        assertTrue(ValidationUtils.isJsonValid(schema,record2));
    }

    @Test
    public final void getJsonSchemaIsValidWithGetRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/get-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/get.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void headJsonSchemaIsValidWithHeadRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/head-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/head.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void putJsonSchemaIsValidWithPutRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/put-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/put.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void deleteJsonSchemaIsValidWithDeleteRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/delete-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/delete.schema.json").getFile());

        assertTrue(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void postJsonSchemaIsNotValidWithPatchRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/patch-record2.json").getFile());
        File schema = new File(classLoader.getResource("schemas/post.schema.json").getFile());

        assertFalse(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void postJsonSchemaIsNotValidWithPutRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/put-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/post.schema.json").getFile());

        assertFalse(ValidationUtils.isJsonValid(schema,record));
    }

    @Test
    public final void getJsonSchemaIsNotValidWithHeadRecord() throws Exception {
        ClassLoader classLoader = new TestMain().getClass().getClassLoader();
        File record = new File(classLoader.getResource("records/head-record.json").getFile());
        File schema = new File(classLoader.getResource("schemas/get.schema.json").getFile());

        assertFalse(ValidationUtils.isJsonValid(schema,record));
    }
}
