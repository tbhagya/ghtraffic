## Introduction
GHTraffic is a dataset comprising HTTP transactions extracted from GitHub data and augmented by synthetic request data. The dataset comprises three different editions of various sizes: Small (S), Medium (M) and Large (L). The S dataset includes HTTP records created from [google/guava](https://github.com/google/guava) repository. Guava is a popular Java library containing utilities and data structures. The M dataset includes records from the [npm/npm](https://github.com/npm/npm) project. It is the popular de-facto standard package manager for JavaScript. The L dataset contains data that were created by selecting eight repositories containing large and very active projects on GitHub as of 2015, including [twbs/bootstrap](https://github.com/twbs/bootstrap), [symfony/symfony](https://github.com/symfony/symfony), [docker/docker](https://github.com/docker/docker), [Homebrew/homebrew](https://github.com/Homebrew/homebrew), [rust-lang/rust](https://github.com/rust-lang/rust), [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes), and [angular/angular.js](https://github.com/angular/angular.js).

This GHTraffic dataset is suitable for reproducible research in service-oriented computing. 

## Accessing GHTraffic Dataset

The different editions of the GHTraffic dataset can be downloaded using: [S](https://zenodo.org/record/1034573/files/ghtraffic-S-1.0.0.zip), [M](https://zenodo.org/record/1034573/files/ghtraffic-M-1.0.0.zip), and [L](https://zenodo.org/record/1034573/files/ghtraffic-L-1.0.0.zip).

## Using GHTraffic Script

We also provide access to the scripts used to generate GHTraffic. Using these scripts, you can modify the configuration properties in the **config.properties** file in order to create a customised version of the GHTraffic dataset for your own use. 

Scripts can be accessed by cloning the repository, or by downloading the pre-configured VirtualBox image from [here](https://zenodo.org/record/1034573/files/ghtraffic-artifact-1.0.0.zip).

### Environment SetUp

The following set up is required to use the GHTraffic script by cloning the repository:

- Install **openjdk-8-jdk**

- Install **MongoDB 3.4.9** 

- Create **data/db** folder where Mongo stores data 

- Download the GHTorrent dump from [here](https://ghtstorage.blob.core.windows.net/downloads/issues-dump.2015-08-04.tar.gz) 

- Move the gzipped tar file to **/data/db** directory and extract (requires nearly 50 GB of memory)

- Move all the content in **/data/db/dump/github/** to **/data/db/github**

- Start the Mongo server by running the following command:

  > sudo service mongod start

- Restore data from the binary database dump to a MongoDB instance by running the following command:

  > mongorestore $path-to-data-folder/data/db/github -d github

### Generating GHTraffic Dataset

- The script **build.sh** with options **S**, **M** or **L** can be used to create either edition. 

- To generate the small edition of GHTraffic, clone the repository into a folder, cd into **script** folder and run the following command:

  > ./build.sh -S  

- To generate all three editions of GHTraffic, run the following command:

  > ./build.sh -S -M -L

- Each dataset will be located in a sub folder called **scripts/DataSet**

- The script **analyse.sh** analyses each dataset in terms of HTTP request methods, status codes and GHTraffic record type.

- Latex tables per matric will be generated in **paper/Metrics** folder

#### Note that due to the use of random data generation this scripts will produce slightly different datasets at each execution.  

## GHTraffic Records

### Data Format

The GHTraffic dataset presents full HTTP request/response messages encoded in JSON format. The structure of the JSON record produced is defined by a JSON schema. There are separate schemas for each HTTP method. These schemas can be found in the repository [schemas](https://bitbucket.org/tbhagya/ghtraffic/src/e398deac6301/schemas/?at=master) folder.

### Sample Records

There are sample GHTraffic records for each method type in the [sample-records](https://bitbucket.org/tbhagya/ghtraffic/src/e398deac6301/schemas/sample-records/?at=master) folder.

### Schema Validation

We have performed a validation process to determine whether the JSON content conforms to the proposed schema definition. The validation tool can be found in the repository [validator](https://bitbucket.org/tbhagya/ghtraffic/src/e398deac6301/schemas/validator/?at=master) folder.

## Versioning of GHTraffic

The original GHTraffic dataset is released as **version 1.0.0**. A new version of the dataset (**version 2.0.0**) is also being produced incorporating minor changes.

Find the project repository from [here](https://bitbucket.org/tbhagya/ghtraffic-version-2.0.0).